# Zadání - The snake
Seznam stránek:
- Samotná aplikace bude mít pravděpodobně jednu hlavní stránku.

Technologie:
- K implementaci zkusím proužít framework p5 (https://p5js.org) a možná další.

Funkce:
- Základní hra Snake
- Přehrávání zvuků
- Ukládání skóre (local storage)
- Nastavení hry (vypnout/zapnout zvuky, game mode (pevné ohraničení - hard/možnost přejít z hrany na hranu - easy, atd..))"

# Moje implementace
*  Cíl projektu
    * Viz zadání.. Ale především jsem si chtěl zkusit vykreslování pomocí p5js. :)
*  Postup
    *  Začaj jsem vytvořením p5 canvasu a snahou zprovoznit pohyb hada po canvasu, když se zdálo že vše funguje, přešel jsem k implementaci game mode (easy/hard), kde jsem musel upravit logiku na hadovi.
    Následoval menu/death screen společně s ukládáním skóre. Pro mě nejtěží část byla responsivita game canvasu, přestože ovládání pomocí swipů (hammerJS) šlo relativně snadno,
     canvas samotný jsem vyřešil nakonec vyřešil pomocí setup metody kde kontroluji podobně jako v css šířku okna a podle ní vytvořim vhodně široký canvas.
*  Popis funkčnosti
    * Jedná se o single page hru vytvořenou pomocí vykreslovacího JS frameworku p5 , dále jsem použil HammerJS (https://hammerjs.github.io/) pro ovládání hry gesty (např na iPadu :))
    a nakonec bootstrap. Ukládání skóre jsem vyřešil za pomocí session storage.